FROM python

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

WORKDIR /app
COPY ./cookbook /app/
COPY requirements.txt /app/

RUN pip install -r requirements.txt

COPY run.sh /app/

RUN chmod +x run.sh

EXPOSE 8000

CMD ["./run.sh"]
