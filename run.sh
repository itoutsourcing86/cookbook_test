#!/bin/bash

python3 manage.py migrate --no-input

if ! python3 manage.py shell -c "from django.contrib.auth.models import User; print(User.objects.filter(username='admin').exists())" | grep -q True;
then
DJANGO_SUPERUSER_PASSWORD=q1w2e3r4t5! python3 manage.py createsuperuser --username admin --email admin@example.com --no-input
fi

python3 manage.py makemigrations --no-input
python3 manage.py migrate --no-input

python manage.py runserver 0.0.0.0:8000
