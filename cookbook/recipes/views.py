from django.shortcuts import render
from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from .models import Recipe, Product, RecipeProduct


def add_product_to_recipe(request):
    weight = request.GET.get('weight')
    if not weight:
        return HttpResponse('Weight is required')

    try:
        weight = float(weight)
    except ValueError:
        return HttpResponse('Wrong weight value')

    if weight <= 0:
        return HttpResponse('Weight must be large than 0')

    recipe = get_object_or_404(Recipe, id=request.GET.get('recipe_id'))

    product = get_object_or_404(Product, id=request.GET.get('product_id'))

    recipe_product, created = RecipeProduct.objects.get_or_create(recipe=recipe, product=product, weight=weight)

    if not created:
        recipe_product.weight = weight
        recipe_product.save()

    return HttpResponse('Product added to recipe successfully')


def cook_recipe(request):
    recipe = get_object_or_404(Recipe, id=request.GET.get('recipe_id'))

    recipe_products = RecipeProduct.objects.filter(recipe=recipe)

    for recipe_product in recipe_products:
        product = recipe_product.product
        product.count += 1
        product.save()

    return HttpResponse("Recipe cooked successfully")


def show_recipes_without_product(request):
    product = get_object_or_404(Product, id=request.GET.get('product_id'))

    recipes = Recipe.objects.exclude(recipeproduct__product=product)

    return render(request, 'recipes.html', {'recipes': recipes})
